<!DOCTYPE html>
<html>
	<head>
		<title>Exemple Appli Spring</title>
		<link rel="stylesheet" href="../../css/bootstrap.min.css" />
	</head>
	<body>
		<div class="container" style="margin-left:10%">
			
			<nav class="navbar navbar-dark bg-primary" style="margin-bottom:30px">
				<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
					<li class="nav-item"><a class="nav-link" href="/">Exemple Appli Spring</a></li>
				</ul>
			</nav>
			
			<h4 class="mt-2" style="margin-bottom:30px"><a href="/simulation_impots">Simulation de l'impôt sur le revenu</a></h4>
			
			<h4 class="mt-2" style="margin-bottom:30px"><a href="/simulation_emprunt_mensualite">Simulation des mensualités d'un emprunt bancaire</a></h4>
			
			<h4 class="mt-2" style="margin-bottom:30px"><a href="/simulation_emprunt_capacite">Simulation de la capacité d'emprunt bancaire</a></h4>
			
		</div>
	</body>
</html>
