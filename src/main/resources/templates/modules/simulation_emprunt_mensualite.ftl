<!DOCTYPE html>
<html>
	<head>
		<title>Exemple Appli Spring</title>
		<link rel="stylesheet" href="../../css/bootstrap.min.css" />
	</head>
	<body>
		<div class="container" style="margin-left:10%">
			
			<nav class="navbar navbar-dark bg-primary" style="margin-bottom:30px">
				<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
					<li class="nav-item"><a class="nav-link" href="/">Exemple Appli Spring</a></li>
				</ul>
			</nav>
			
			<h4 class="mt-2" style="margin-bottom:30px">Simulation des mensualités d'un emprunt bancaire</h4>
			
			<#if res?? >
			
			<div style="margin-left:5%">
				<p style="margin-bottom:15px">Résultats de la simulation</p>
				<table class="table table-striped">
					<thead>
						<tr>
							<th scope="col">Paramètre</th>
							<th scope="col">Valeur</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Montant emprunté</td>
							<td>${montant} euros</td>
						</tr>
						<tr>
							<td>Durée de l'emprunt</td>
							<td>${duree} ans</td>
						</tr>
						<tr>
							<td>Taux d'emprunt</td>
							<td>${taux} %</td>
						</tr>
						<tr>
							<td>Mensualité à rembourser</td>
							<td>${res} euros</td>
						</tr>
					</tbody>
				</table>
				
				<p style="margin-top:15px">Pour retourner à la simulation <a href="">cliquez ici</a></p>
				
			</div>
			
			<#else>
			
			<div style="margin-left:5%">
			<form class="needs-validation" action="" method="post">
				
				<div class="form-group">
					<label for="montant">Montant emprunté:</label><br/>
					<input id="montant" type="number" min="0" name="montant" placeholder="Montant en euros" required/>
				</div>
				
				<div class="form-group">
					<label for="duree">Durée de l'emprunt (en années):</label><br/>
					<input id="duree" type="number" min="0" name="duree" placeholder="Nombre d'années" required/>
				</div>
				
				<div class="form-group">
					<label for="taux">Taux d'emprunt (en pourcentage):</label><br/>
					<input id="taux" type="number" min="0" step="0.01" name="taux" placeholder="Ex: 2,25" required/> %
				</div>
				
				<button type="submit" class="btn btn-primary">Calculer la mensualité</button>
			</form>
			</div>
			
			
			</#if>
		</div>
	</body>
</html>
