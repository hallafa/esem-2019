package com.training.samplespringapp.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/simulation_emprunt_capacite")
public class SimulationEmpruntCapaciteController {

	@GetMapping(value={"", "/"})
	public String get() {
		return "modules/simulation_emprunt_capacite";
	}

	@PostMapping(value={"", "/"})
	public String submit(@ModelAttribute("montant") Integer montant,
						@ModelAttribute("duree") Integer duree,
						@ModelAttribute("taux") Double taux,
						Model model) {
		double res = calculCapaciteEmprunt(montant, duree, taux);
		model.addAttribute("montant", montant);
		model.addAttribute("duree", duree);
		model.addAttribute("taux", taux);
		if (res >= 0) {
			model.addAttribute("res", res);
		}
		else {
			model.addAttribute("res", "Fonctionnalité à implémenter");
		}
		return "modules/simulation_emprunt_capacite";
	}

	/*
		Formule de calcul: https://fr.wikipedia.org/wiki/Mensualit%C3%A9#Exemple
	*/
	public double calculCapaciteEmprunt(int montant, int duree, double taux) {
		/*
		TODO: a implémenter
		*/
		return -1;
	}

}
