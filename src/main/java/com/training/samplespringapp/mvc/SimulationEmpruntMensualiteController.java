package com.training.samplespringapp.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/simulation_emprunt_mensualite")
public class SimulationEmpruntMensualiteController {

	@GetMapping(value={"", "/"})
	public String get() {
		return "modules/simulation_emprunt_mensualite";
	}

	@PostMapping(value={"", "/"})
	public String submit(@ModelAttribute("montant") Integer montant,
						@ModelAttribute("duree") Integer duree,
						@ModelAttribute("taux") Double taux,
						Model model) {
		double res = calculMensualiteEmprunt(montant, duree, taux);
		model.addAttribute("montant", montant);
		model.addAttribute("duree", duree);
		model.addAttribute("taux", taux);
		model.addAttribute("res", res);
		return "modules/simulation_emprunt_mensualite";
	}

	/*
		Formule de calcul: https://fr.wikipedia.org/wiki/Mensualit%C3%A9#Exemple
	*/
	public double calculMensualiteEmprunt(int montant, int duree, double taux) {
		taux = taux / 100;
		double t = Math.pow(1 + taux / 12, duree * 12);
		double res = (montant * taux) / (12 * (1 - 1 / t));
		return res;
	}

}
